"""
App Module
==========
 
Description Module
------------------
 
Ce module permet d'une part de gérer l'interface de l'app 
Salad'Cesar puis d'autre part comporte une partie de fonctionnalité
permettant le chiffrage et déchiffrage d'un message.
"""

from tkinter.messagebox import *
from tkinter import *
from chiffrement import *

##
# Definition de l'execution des boutons
def button_chiffrer_validation():
    """
    Description FCT BTN_chiffrer
    ============================

    Cette fonction permet l'appel de la FCT chiffrement avec un 
    offset positif.
    """
    zone_text_res.delete('0.0', END)
    str = get_message(zone_text_saisi)
    res = chiffrement(str, int(box_pas.get()), var.get())
    zone_text_res.insert(INSERT, res)
    showinfo('Message Chiffre', 'Chiffrage de votre message réalisé ! ')

def button_dechiffrer_validation():
    """
    Description FCT BTN_déchiffrer
    ============================

    Cette fonction permet l'appel de la FCT déchiffrement avec un 
    offset négatif.
    """
    zone_text_saisi.delete('0.0', END)
    str = get_message(zone_text_res)
    res = chiffrement(str, -int(box_pas.get()), var.get())
    zone_text_saisi.insert(INSERT, res)
    showinfo('Message Dechiffre', 'Déchiffrage de votre code réalisé ! ')

##
# Initialisation de l'interface graphique
root = Tk()
icon = PhotoImage(file='logo.png')
root.title("Salad'Cesar")
root.iconphoto(False, icon)
#root.maxsize(1285, 500)
#root.minsize(1285, 500)

##
# Init de la zone de text où l'on inscrira un message / code 

label_saisi = Label(root, text = "Message")
label_saisi.grid(column=0, row=0)

zone_text_saisi = Text(root)
zone_text_saisi.grid(column=0, row=1)

label_saisi = Label(root, text = "Code")
label_saisi.grid(column=1, row=0)

zone_text_res = Text(root)
zone_text_res.grid(column=1, row=1)

chiffrer_button=Button(root, text="Chiffrer", command=button_chiffrer_validation)
chiffrer_button.grid(column=0, row=2, sticky='nsew')

dechiffrer_button=Button(root, text="Dechiffrer", command=button_dechiffrer_validation)
dechiffrer_button.grid(column=1, row=2, sticky='nsew')

label_saisi = Label(root, text = "Offset chiffrage")
label_saisi.grid(columnspan=2, row=3)

box_pas = Spinbox(root, from_=0, to=10)
box_pas.grid(columnspan=2, row=4)

var = IntVar()
check_non_circulaire = Checkbutton(root, text="Chiffrement de type giratoire", variable = var)
check_non_circulaire.grid(columnspan=2, row=5)

root.mainloop()
## Projet Salad'Cesar 

> #### Contexte
> > Ce projet consiste à reproduire la technique de chiffrement utilisé par César en son temps qui servait donc à coder des messages importants qu'il devait envoyer à divers membre de l'Empire Romain.  

> #### TODO
> > - Réaliser une approche TDD du projet
> > - Devlopper les fonctions de notre App
> > - Devlopper avec Tkinter une interface pour notre App

```
Projet réalisé par TRICOT Yann, CRAMEZ Théo et DORION William DUT2 INFO 2019/2020
```


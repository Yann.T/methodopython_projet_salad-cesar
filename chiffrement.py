from tkinter import *

def chiffrement(string, offset, circulaire):
    """
    Description FCT chiffrement
    ===========================

    Cette fonction permet de chiffrer ou déchiffrer un message (string)
    passé en paramètre avec un offset positif pour chiffrer et négatif
    pour déchiffrer.
    """
    tabchar = split_message(string)
    outstr = ""
    for char in tabchar:
        if char != '\n':
            if circulaire:
                char = chiffrement_circulaire(ord(char), offset)
                outstr = outstr + chr(char)
            else:
                outstr = outstr + chr(ord(char) + offset)
    return outstr

def chiffrement_circulaire(char_fournis, offset):
    """
    Description FCT chiffrement_circulaire
    ======================================

    Cette fonction permet de gérer le chiffrement circulaire du code
    de César, c'est à dire que lorsqu'on dépasse notre limite d'alphabet 
    (Latin) on revient au début de celui-ci pour chiffrer.
    """
    char_fournis = chiffrement_circulaire_casse(char_fournis, 64, 91, offset)
    char_fournis = chiffrement_circulaire_casse(char_fournis, 96, 123, offset)
    return char_fournis

def chiffrement_circulaire_casse(char_fournis, pos_min, pos_max, offset): #96 123
    """
    Description de la FCT chiffrement_circulaire_casse
    ==================================================

    Cette fonction à pour but d'éviter la redondance de code
    de sa FCT mère chiffrement_circulaire, en prenant en 
    paramètre la lettre fournie, la position minimale de la
    casse fournie, maximale et l'offset.
    """
    
    if char_fournis > pos_min and char_fournis < pos_max:
        char_fournis = char_fournis + offset
        if char_fournis >= pos_max:
            char_fournis = (char_fournis % (pos_max-1)) + pos_min
        if char_fournis <= pos_min:
            char_fournis = char_fournis + 26
    return char_fournis

def get_message(Text):
    """
    Description FCT get_Message
    ===========================

    Cette fonction permet de recupérer dans une string le contenu de la TextArea
    soit Message ou Code en fonction du paramètre passé.
    """
    str = Text.get("0.0", END)
    return str

def split_message(str):
    """
    Description FCT split_message
    =============================

    Cette fonction permet de retourner une list constituer des caractères d'une
    string passée en paramètre.
    """
    return list(str)